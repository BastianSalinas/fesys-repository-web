﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="FESYS.Presentation.Employee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="alert alert-info">
     <h2>Ingreso de Empleado</h2>
    <asp:Label ID="Label1" runat="server" Text="Label">Rut:</asp:Label>
     <asp:TextBox ID="txtRutE" runat="server" Height="24px" Width="400px" ></asp:TextBox>
     <asp:Label ID="Label2" runat="server" Text="Label">DV:</asp:Label>
     <asp:TextBox ID="txtDVE" runat="server" Height="24px" Width="60px"></asp:TextBox>
        <br /><br />
     <asp:Label ID="Label3" runat="server" Text="Label">Nombre:</asp:Label>
     <asp:TextBox ID="txtNameE" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label4" runat="server" Text="Label">Apellido:</asp:Label>
     <asp:TextBox ID="txtLastNameE" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label6" runat="server" Text="Label">Tipo de Cliente</asp:Label>
     <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control">
     </asp:DropDownList>
     <asp:Label ID="lblMessages" ForeColor="Red" runat="server" Text=""></asp:Label>

     <br />
     <asp:Button ID="txtCreate" runat="server" Text="Ingresar" />
     <asp:Button ID="txtShow" runat="server" Text="Mostrar" />
     <asp:Button ID="txtUpdate" runat="server" Text="Modificar" />
     <asp:Button ID="txtDelete" runat="server" Text="Eliminar" />
     </div>
</asp:Content>
