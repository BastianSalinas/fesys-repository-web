﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Sale.aspx.cs" Inherits="FESYS.Presentation.Sale" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="alert alert-info">
     <h2>Ingreso de Venta</h2>
     <asp:Label ID="Label3" runat="server" Text="Label">Fecha:</asp:Label>
     <asp:TextBox ID="txtDate" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label4" runat="server" Text="Label">Monto Total:</asp:Label>
     <asp:TextBox ID="txtTotal" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label6" runat="server" Text="Label">Cliente</asp:Label>
     <asp:DropDownList ID="ddlCustomer" runat="server" class="form-control">
     </asp:DropDownList>
     <asp:Label ID="Label1" runat="server" Text="Label">Empleado</asp:Label>
     <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control">
     </asp:DropDownList>
     <asp:Label ID="Label2" runat="server" Text="Label">Boleta</asp:Label>
     <asp:DropDownList ID="ddlTicket" runat="server" class="form-control">
     </asp:DropDownList>
     <asp:Label ID="Label5" runat="server" Text="Label">Factura</asp:Label>
     <asp:DropDownList ID="ddlInvoice" runat="server" class="form-control">
     </asp:DropDownList>
     <asp:Label ID="Label7" runat="server" Text="Label">Empresa: </asp:Label>
     <asp:DropDownList ID="ddlCompany" runat="server" class="form-control">
     </asp:DropDownList>
     <asp:Label ID="lblMessages" ForeColor="Red" runat="server" Text=""></asp:Label>
     
    <br />
     <asp:Button ID="txtCreate" runat="server" Text="Ingresar" />
     <asp:Button ID="txtShow" runat="server" Text="Mostrar" />
     <asp:Button ID="txtUpdate" runat="server" Text="Modificar" />
     <asp:Button ID="txtDelete" runat="server" Text="Eliminar" />
     </div>
</asp:Content>
