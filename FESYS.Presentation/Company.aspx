﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Company.aspx.cs" Inherits="FESYS.Presentation.Company" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="alert alert-info">
     <h2>Ingreso de Empresa</h2>
     
     <asp:Label ID="Label1" runat="server" Text="Label">Rut Empresa:</asp:Label>
     <asp:TextBox ID="txtRutC" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label2" runat="server" Text="Label">Nombre Empresa:</asp:Label>
     <asp:TextBox ID="txtNameC" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label3" runat="server" Text="Label">Rubro:</asp:Label>
     <asp:TextBox ID="txtEntry" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label4" runat="server" Text="Label">Dirección:</asp:Label>
     <asp:TextBox ID="txtAddressC" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label5" runat="server" Text="Label">Telefono</asp:Label>
     <asp:TextBox ID="txtPhoneC" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label7" runat="server" Text="Label">Email:</asp:Label>
     <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
     <asp:Label ID="Label6" runat="server" Text="Label">Tipo de Empresa</asp:Label>
     <asp:DropDownList ID="ddlCompany" runat="server" class="form-control">
     </asp:DropDownList>
     <asp:Label ID="lblMessages" ForeColor="Red" runat="server" Text=""></asp:Label>
     <br />
     <asp:Button ID="txtCreate" runat="server" Text="Ingresar" />
     <asp:Button ID="txtShow" runat="server" Text="Mostrar" />
     <asp:Button ID="txtUpdate" runat="server" Text="Modificar" />
     <asp:Button ID="txtDelete" runat="server" Text="Eliminar" />
    </div>
</asp:Content>
