﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FESYS.Data;
using System.Data.Entity;


namespace FESYS.Business
{
    public class Employee
    {
        #region Atributos
        public decimal Id_Employee { get; set; }
        public string Rut_Employee { get; set; }
        public string Dv_Employee { get; set; }
        public string Name_Employee { get; set; }
        public string Lastname_Employee { get; set; }
        public decimal Type_Employee_Id_Type_Employee { get; set; }
        #endregion

        #region Constructores
        public Employee()
        {

        }
        public Employee(decimal id_Employee, string rut_Employee, string dv_Employee, string name_Employee, string lastname_Employee, decimal type_Employee_Id_Type_Employee)
        {
            Id_Employee = id_Employee;
            Rut_Employee = rut_Employee;
            Dv_Employee = dv_Employee;
            Name_Employee = name_Employee;
            Lastname_Employee = lastname_Employee;
            Type_Employee_Id_Type_Employee = type_Employee_Id_Type_Employee;
        }
        #endregion

        #region Métodos

        public bool Ingresar()
        {
            try
            {
                var salida = new System.Data.Entity.Core.Objects.ObjectParameter("v_salida", typeof(string));
                Connection._database.PA_INGRESAR_EMPLEADO(Rut_Employee, Dv_Employee, Name_Employee, Lastname_Employee, Type_Employee_Id_Type_Employee, salida);
                if (salida.ToString() == "exito")
                {
                    
                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        #endregion
    }
}
