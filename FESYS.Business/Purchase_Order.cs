﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Purchase_Order
    {
        #region Atributos
        public decimal Id_Order { get; set; }
        public string date { get; set; }
        public string Rut_Company { get; set; }
        public string Office { get; set; }
        public string Address_Office { get; set; }
        public string Phone_Office { get; set; }
        public string Employee { get; set; }
        public string Supplier { get; set; }
        public string Address_Supplier { get; set; }
        public string Phone_Supplier { get; set; }
        public string Status { get; set; }
        public string Product { get; set; }
        public decimal Quantity { get; set; }
        public string UM_Product { get; set; }
        public decimal Price { get; set; }
        public decimal Net { get; set; }
        public decimal IVA { get; set; }
        public decimal Total { get; set; }
        public decimal Supplier_Id_Supplier { get; set; }
        #endregion

        #region Constructores
        public Purchase_Order()
        {

        }
        public Purchase_Order(decimal id_Order, string date, string rut_Company, string office, string address_Office, string phone_Office, string employee, string supplier, string address_Supplier, string phone_Supplier, string status, string product, decimal quantity, string uM_Product, decimal price, decimal net, decimal iVA, decimal total, decimal supplier_Id_Supplier)
        {
            Id_Order = id_Order;
            this.date = date;
            Rut_Company = rut_Company;
            Office = office;
            Address_Office = address_Office;
            Phone_Office = phone_Office;
            Employee = employee;
            Supplier = supplier;
            Address_Supplier = address_Supplier;
            Phone_Supplier = phone_Supplier;
            Status = status;
            Product = product;
            Quantity = quantity;
            UM_Product = uM_Product;
            Price = price;
            Net = net;
            IVA = iVA;
            Total = total;
            Supplier_Id_Supplier = supplier_Id_Supplier;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
