﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Invoice
    {
        #region Atributos
        public decimal Id_Invoice { get; set; }
        public string Date_Invoice { get; set; }
        public string Rut_Company { get; set; }
        public string Office { get; set; }
        public string Entry { get; set; }
        public string Employee { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Rut_Customer { get; set; }
        public decimal Net { get; set; }
        public decimal IVA { get; set; }
        public decimal Total_Sale { get; set; }
        #endregion

        #region Constructores
        public Invoice()
        {

        }

        public Invoice(decimal id_Invoice, string date_Invoice, string rut_Company, string office, string entry, string employee, string email, string phone, string rut_Customer, decimal net, decimal iVA, decimal total_Sale)
        {
            Id_Invoice = id_Invoice;
            Date_Invoice = date_Invoice;
            Rut_Company = rut_Company;
            Office = office;
            Entry = entry;
            Employee = employee;
            Email = email;
            Phone = phone;
            Rut_Customer = rut_Customer;
            Net = net;
            IVA = iVA;
            Total_Sale = total_Sale;
        }

        #endregion

        #region Métodos
        public bool DSFDF()
        {

            return false;
        }
        #endregion
    }
}
