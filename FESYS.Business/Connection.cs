﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FESYS.Data;

namespace FESYS.Business
{
    //Se hace la referencia a FESYS.Data para tener acceso a los datos de la bd, además
    //Se agregó el nugget de EntitiFramework última versión 6.2 en FESYS.Data y FESYS.Business
    //Finalmente se copio App.Config desde FESYS.Data a FESYS.Business
    public class Connection
    {
        public static FESYS.Data.FESYSEntities _database;

        public static FESYS.Data.FESYSEntities DataBase
        {
            get
            {
                if(_database==null)
                {
                    _database = new FESYS.Data.FESYSEntities();
                }
                return _database;
            }
        }

        public Connection() { }
    }
}
