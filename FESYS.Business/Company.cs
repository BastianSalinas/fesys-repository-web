﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Company
    {
        #region Atributos
        public decimal Id_Company { get; set; }
        public string Rut_Company { get; set; }
        public string Name_Company { get; set; }
        public string Entry_Company { get; set; }
        public string Address_Company { get; set; }
        public string Phone_Company { get; set; }
        public string Email { get; set; }
        #endregion

        #region Constructores
        public Company()
        {

        }

        public Company(decimal id_Company, string rut_Company, string name_Company, string entry_Company, string address_Company, string phone_Company, string email)
        {
            Id_Company = id_Company;
            Rut_Company = rut_Company;
            Name_Company = name_Company;
            Entry_Company = entry_Company;
            Address_Company = address_Company;
            Phone_Company = phone_Company;
            Email = email;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
