﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Type_Customer
    {
        #region Atributos
        public decimal Id_Type_Customer { get; set; }
        public string Type_Customer1 { get; set; }
        public string Desc_Type_Customer { get; set; }
        #endregion

        #region Constructores
        public Type_Customer()
        {

        }
        public Type_Customer(decimal id_Type_Customer, string type_Customer1, string desc_Type_Customer)
        {
            Id_Type_Customer = id_Type_Customer;
            Type_Customer1 = type_Customer1;
            Desc_Type_Customer = desc_Type_Customer;
        }

        #endregion

        #region Métodos
        #endregion

    }
}
