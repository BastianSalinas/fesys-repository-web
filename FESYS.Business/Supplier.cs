﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Supplier
    {
        #region Atributos
        public decimal Id_Supplier { get; set; }
        public string Rut_Supplier { get; set; }
        public string Name_Supplier { get; set; }
        public string Entry_Supplier { get; set; }
        public string Address_Supplier { get; set; }
        public string Phone_Supplier { get; set; }
        #endregion

        #region Constructores
        public Supplier()
        {

        }
        public Supplier(decimal id_Supplier, string rut_Supplier, string name_Supplier, string entry_Supplier, string address_Supplier, string phone_Supplier)
        {
            Id_Supplier = id_Supplier;
            Rut_Supplier = rut_Supplier;
            Name_Supplier = name_Supplier;
            Entry_Supplier = entry_Supplier;
            Address_Supplier = address_Supplier;
            Phone_Supplier = phone_Supplier;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
