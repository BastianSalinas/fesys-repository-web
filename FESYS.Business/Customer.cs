﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Customer
    {
        #region Atributos
        public decimal Id_Customer { get; set; }
        public string Rut_Customer { get; set; }
        public string Name_Customer { get; set; }
        public string Lastname_Customer { get; set; }
        public string Address_Customer { get; set; }
        public string Phone_Customer { get; set; }
        public decimal Type_Customer_Id_Type_Customer { get; set; }
        #endregion

        #region Constructores
        public Customer()
        {

        }
        public Customer(decimal id_Customer, string rut_Customer, string name_Customer, string lastname_Customer, string address_Customer, string phone_Customer, decimal type_Customer_Id_Type_Customer)
        {
            Id_Customer = id_Customer;
            Rut_Customer = rut_Customer;
            Name_Customer = name_Customer;
            Lastname_Customer = lastname_Customer;
            Address_Customer = address_Customer;
            Phone_Customer = phone_Customer;
            Type_Customer_Id_Type_Customer = type_Customer_Id_Type_Customer;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
