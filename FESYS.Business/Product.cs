﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Product
    {
        #region Atributos
        public decimal Id_Product { get; set; }
        public string Name_Product { get; set; }
        public string Desc_Product { get; set; }
        public string Trademark { get; set; }
        public decimal Price_Product { get; set; }
        public string UM_Product { get; set; }
        public decimal Stock { get; set; }
        public decimal Stock_Critic { get; set; }
        public string Expiry_Product { get; set; }
        public string Image { get; set; }
        public string Source { get; set; }
        public decimal Type_Product_Id_Type_Product { get; set; }
        public decimal Purchase_Order_Id_Order { get; set; }
        #endregion

        #region Constructores

        public Product()
        {

        }
        public Product(decimal id_Product, string name_Product, string desc_Product, string trademark, decimal price_Product, string uM_Product, decimal stock, decimal stock_Critic, string expiry_Product, string image, string source, decimal type_Product_Id_Type_Product, decimal purchase_Order_Id_Order)
        {
            Id_Product = id_Product;
            Name_Product = name_Product;
            Desc_Product = desc_Product;
            Trademark = trademark;
            Price_Product = price_Product;
            UM_Product = uM_Product;
            Stock = stock;
            Stock_Critic = stock_Critic;
            Expiry_Product = expiry_Product;
            Image = image;
            Source = source;
            Type_Product_Id_Type_Product = type_Product_Id_Type_Product;
            Purchase_Order_Id_Order = purchase_Order_Id_Order;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
