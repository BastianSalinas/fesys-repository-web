﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Detail_Sale
    { 
        #region Atributos
        public decimal Id_Detail_Sale { get; set; }
        public string Product_Detail_Sale { get; set; }
        public decimal Price_Detail_Sale { get; set; }
        public decimal Quantity_Detail_Sale { get; set; }
        public decimal Sub_Total_Detail_Sale { get; set; }
        public decimal Sale_Id_Sale { get; set; }
        public decimal Product_Id_Product { get; set; }
        #endregion

        #region Constructores
        public Detail_Sale()
        {

        }
        public Detail_Sale(decimal id_Detail_Sale, string product_Detail_Sale, decimal price_Detail_Sale, decimal quantity_Detail_Sale, decimal sub_Total_Detail_Sale, decimal sale_Id_Sale, decimal product_Id_Product)
        {
            Id_Detail_Sale = id_Detail_Sale;
            Product_Detail_Sale = product_Detail_Sale;
            Price_Detail_Sale = price_Detail_Sale;
            Quantity_Detail_Sale = quantity_Detail_Sale;
            Sub_Total_Detail_Sale = sub_Total_Detail_Sale;
            Sale_Id_Sale = sale_Id_Sale;
            Product_Id_Product = product_Id_Product;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
