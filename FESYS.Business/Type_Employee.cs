﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Type_Employee
    {
        #region Atributos
        public decimal Id_Type_Employee { get; set; }
        public string Job { get; set; }
        public string Desc_Job { get; set; }
        #endregion

        #region Constructores
        public Type_Employee()
        {

        }
        public Type_Employee(decimal id_Type_Employee, string job, string desc_Job)
        {
            Id_Type_Employee = id_Type_Employee;
            Job = job;
            Desc_Job = desc_Job;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
