﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Collection
    {
        public List<Type_Customer> GetTypeCustomer()
        {
            List<Type_Customer> output = new List<Type_Customer>();
            foreach (Data.TYPE_CUSTOMER custo in Connection.DataBase.TYPE_CUSTOMER)
            {
                output.Add(new Type_Customer()
                {
                    Id_Type_Customer = custo.ID_TYPE_CUSTOMER,
                    Type_Customer1 = custo.TYPE_CUSTOMER1,
                    Desc_Type_Customer = custo.DESC_TYPE_CUSTOMER
                });
            }
            return output;
        }
        public List<Type_Employee> GetTypeEmployee()
        {
            List<Type_Employee> output = new List<Type_Employee>();
            foreach (Data.TYPE_EMPLOYEE temp in Connection.DataBase.TYPE_EMPLOYEE)
            {
                output.Add(new Type_Employee()
                {
                    Id_Type_Employee = temp.ID_TYPE_EMPLOYEE,
                    Job = temp.JOB,
                    Desc_Job = temp.DESC_JOB
                });
            }
            return output;
        }
        public List<Office> GetOffice()
        {
            List<Office> output = new List<Office>();
            foreach (Data.OFFICE off in Connection.DataBase.OFFICE)
            {
                output.Add(new Office()
                {
                    Id_Office = off.ID_OFFICE,
                    Name_Office = off.NAME_OFFICE,
                    Address_Office = off.ADDRESS_OFFICE,
                    Phone_Office = off.PHONE_OFFICE,
                    Company_Id_Company = off.COMPANY_ID_COMPANY  
                });
            }
            return output;
        }
    }
}
