﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Ticket
    {
        #region Atributos
        public decimal Id_Ticket { get; set; }
        public System.DateTime Date_Ticket { get; set; } 
        public string Office { get; set; }
        public string Employee { get; set; }
        public string Product { get; set; }
        public string Quantity { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Price { get; set; }
        public decimal Total_Sale { get; set; }
        #endregion

        #region Constructores
        public Ticket()
        {

        }

        public Ticket(decimal id_Ticket, DateTime date_Ticket, string office, string employee, string product, string quantity, string description, decimal? price, decimal total_Sale)
        {
            Id_Ticket = id_Ticket;
            Date_Ticket = date_Ticket;
            Office = office;
            Employee = employee;
            Product = product;
            Quantity = quantity;
            Description = description;
            Price = price;
            Total_Sale = total_Sale;
        }


        #endregion

        #region Métodos
        #endregion
    }
}
