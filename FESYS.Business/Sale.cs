﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Sale
    {
        #region Atributos
        public decimal Id_Sale { get; set; }
        public string Date_Sale { get; set; }
        public decimal Total_Sale { get; set; }
        public decimal Customer_Id_Customer { get; set; }
        public decimal Employee_Id_Employee { get; set; }
        public decimal Ticket_Id_Ticket { get; set; }
        public decimal Invoice_Id_Invoice { get; set; }
        public decimal Company_Id_Company { get; set; }
        #endregion

        #region Constructores
        public Sale()
        {

        }

        public Sale(decimal id_Sale, string date_Sale, decimal total_Sale, decimal customer_Id_Customer, decimal employee_Id_Employee, decimal ticket_Id_Ticket, decimal invoice_Id_Invoice, decimal company_Id_Company)
        {
            Id_Sale = id_Sale;
            Date_Sale = date_Sale;
            Total_Sale = total_Sale;
            Customer_Id_Customer = customer_Id_Customer;
            Employee_Id_Employee = employee_Id_Employee;
            Ticket_Id_Ticket = ticket_Id_Ticket;
            Invoice_Id_Invoice = invoice_Id_Invoice;
            Company_Id_Company = company_Id_Company;
        }

        #endregion

        #region Métodos
        #endregion
    }
}
