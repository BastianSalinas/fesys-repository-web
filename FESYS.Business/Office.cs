﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Office
    {
        #region Atributos
        public decimal Id_Office { get; set; }
        public string Name_Office { get; set; }
        public string Address_Office { get; set; }
        public string Phone_Office { get; set; }
        public decimal Company_Id_Company { get; set; }
        #endregion

        #region Constructores

        public Office()
        {

        }
        public Office(decimal id_Office, string name_Office, string address_Office, string phone_Office, decimal company_Id_Company)
        {
            Id_Office = id_Office;
            Name_Office = name_Office;
            Address_Office = address_Office;
            Phone_Office = phone_Office;
            Company_Id_Company = company_Id_Company;
        }

        #endregion

        #region Métodos
        #endregion
    }
}
