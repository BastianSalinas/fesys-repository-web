﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESYS.Business
{
    public class Type_Product
    {
        #region Atributos
        public decimal Id_Type_Product { get; set; }
        public string Name_Type_Product { get; set; }
        public string Desc_Type_Product { get; set; }
        #endregion

        #region Constructores
        public Type_Product()
        {

        }
        public Type_Product(decimal id_Type_Product, string name_Type_Product, string desc_Type_Product)
        {
            Id_Type_Product = id_Type_Product;
            Name_Type_Product = name_Type_Product;
            Desc_Type_Product = desc_Type_Product;
        }
        #endregion

        #region Métodos
        #endregion
    }
}
